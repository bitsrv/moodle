<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" encoding="utf-8" indent="yes" omit-xml-declaration="yes"/>
    
    <xsl:template match="div">
        <xsl:if test="contains(@class, 'frontpage')">
        <div>
            <xsl:attribute name="class">
                <xsl:value-of select="@class" disable-output-escaping="yes"/>
            </xsl:attribute>
            <xsl:if test="count(//div[@class='coursebox']) &gt; 0">
                <xsl:apply-templates select="//div[@class='coursebox']">
                    <xsl:with-param name="group-size" select="3"/>
                </xsl:apply-templates>
            </xsl:if>
        </div>
        </xsl:if>
        <xsl:if test="not(contains(@class, 'frontpage'))">
        <div>
            <xsl:attribute name="class">
                <xsl:value-of select="@class" disable-output-escaping="yes"/>
            </xsl:attribute>
            <xsl:if test="count(//div[@class='coursebox']) &gt; 0">
                <xsl:apply-templates select="//div[@class='coursebox']">
                    <xsl:with-param name="group-size" select="4"/>
                </xsl:apply-templates>
            </xsl:if>
        </div>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="//div[@class='coursebox']">
        <xsl:param name="group-size" />
        <xsl:if test="position() mod $group-size = 1">
            <div class="row-fluid">
                <xsl:apply-templates select=".|following-sibling::div[@class='coursebox' and position() &lt; $group-size]" mode="list">
                    <xsl:with-param name="group-size" select="$group-size"/>
                </xsl:apply-templates>
            </div>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="//div[@class='coursebox']" mode="list">
        <xsl:param name="group-size" />
        <xsl:if test="$group-size = 3">
            <div class="coursebox span4">
                <xsl:copy-of select="child::*"/>
            </div>
        </xsl:if>
        <xsl:if test="$group-size = 4">
            <div class="coursebox span3">
                <xsl:copy-of select="child::*"/>
            </div>
        </xsl:if>
    </xsl:template>
    
</xsl:stylesheet>
