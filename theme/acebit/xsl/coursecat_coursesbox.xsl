<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8" indent="yes" omit-xml-declaration="yes"/>
<xsl:param name="default_course_cover" />
<xsl:template match="/">
    <div class="coursebox">
        <div class="courseimage">
            <a>
                <xsl:attribute name="href">
                    <xsl:value-of select="//h3/a/@href"/>
                </xsl:attribute>
                <xsl:choose>
                    <xsl:when test="count(//div[@class='courseimage']/img) &gt; 0">
                        <xsl:copy-of select="//div[@class='courseimage']/img"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <img>
                            <xsl:attribute name="src">
                                <xsl:value-of select="$default_course_cover"/>
                            </xsl:attribute>
                        </img>
                    </xsl:otherwise>
                </xsl:choose>
            </a>
        </div>
        <xsl:if test="count(//h3/a) &gt; 0">
            <div class="course-name">
                <xsl:value-of select="//h3/a"/>
            </div>
        </xsl:if>
        <xsl:if test="count(//ul/li/a) &gt; 0">
            <div class="teachers">
                <small><xsl:copy-of select="//ul/li/a" /></small>
            </div>
        </xsl:if>
    </div>
</xsl:template>

</xsl:stylesheet>
