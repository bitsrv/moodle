//Message
YUI({ filter:'raw' }).use("io-xdr", "json-parse", "node", "handlebars", "node-base",
    function(Y) {

        //Event handler called when the transaction begins:
        var handleStart = function(id, a) {
        }

        //Event handler for the success event -- use this handler to write the fetched
        //RSS items to the page.
        var handleSuccess = function(id, o, a) {
            //We use JSON.parse to sanitize the JSON (as opposed to simply performing an
            //JavaScript eval of the data):
            var result = Y.JSON.parse(o.responseText);
            moment.lang('zh-cn');

            var i;
            for (i = 0; i < result.messages.length; i++) {
                result.messages[i].fromNow = moment(new Date(result.messages[i].time)).fromNow();
            }

            // Extract the template string and compile it into a reusable function.
            var source   = Y.one('#header-message-template').getHTML(),
                template = Y.Handlebars.compile(source),
                html;

            // Render the template to HTML using the specified data.
            html = template(result);

            // Append the rendered template to the page.
            Y.one('#headerMessage').setHTML(html);
        }

        //In the event that the HTTP status returned does not resolve to,
        //HTTP 2xx, a failure is reported and this function is called:
        var handleFailure = function(id, o, a) {
            Y.log("ERROR " + id + " " + a, "info", "message");
        }

        //With all the apparatus in place, we can now configure our
        //IO call.  The method property is defined, but if omitted,
        //IO will default to HTTP GET.
        var cfg = {
            method: "GET",
            xdr: {
                use:'native'
            },
            on: {
                //Our event handlers previously defined:
                start: handleStart,
                success: handleSuccess,
                failure: handleFailure
            }
        };

        //Wire the button to a click handler to fire our request each
        //time the button is clicked:
        var listMessages = function(o) {
            Y.log("Load unread message count", "info", "message");

            // Remove the default "X-Requested-With" header as this will
            // prevent the request from succeeding; the Pipes
            // resource will not accept user-defined HTTP headers.
            Y.io.header('X-Requested-With');

            var obj = Y.io(
                //this is a specific Pipes feed, populated with cycling news:
                "/moodle/theme/acebit/api/ccs.php?action=listMessages",
                cfg
            );
        }

        listMessages();
        window.setInterval(listMessages, 60000);
    }
);

// Invitation
YUI({ filter:'raw' }).use("io-xdr", "json-parse", "node", "handlebars", "node-base",
    function(Y) {

        //Event handler called when the transaction begins:
        var handleStart = function(id, a) {
        }

        //Event handler for the success event -- use this handler to write the fetched
        //RSS items to the page.
        var handleSuccess = function(id, o, a) {
            //We use JSON.parse to sanitize the JSON (as opposed to simply performing an
            //JavaScript eval of the data):
            var result = Y.JSON.parse(o.responseText);

            // Extract the template string and compile it into a reusable function.
            var source   = Y.one('#header-request-template').getHTML(),
                template = Y.Handlebars.compile(source),
                html;

            // Render the template to HTML using the specified data.
            html = template(result);

            // Append the rendered template to the page.
            Y.one('#headerRequest').setHTML(html);
        }

        //In the event that the HTTP status returned does not resolve to,
        //HTTP 2xx, a failure is reported and this function is called:
        var handleFailure = function(id, o, a) {
            Y.log("ERROR " + id + " " + a, "info", "message");
        }

        //With all the apparatus in place, we can now configure our
        //IO call.  The method property is defined, but if omitted,
        //IO will default to HTTP GET.
        var cfg = {
            method: "GET",
            xdr: {
                use:'native'
            },
            on: {
                //Our event handlers previously defined:
                start: handleStart,
                success: handleSuccess,
                failure: handleFailure
            }
        };

        //Wire the button to a click handler to fire our request each
        //time the button is clicked:
        var countFriendInvitation = function(o) {
            Y.log("Load friend invitation count", "info", "message");

            // Remove the default "X-Requested-With" header as this will
            // prevent the request from succeeding; the Pipes
            // resource will not accept user-defined HTTP headers.
            Y.io.header('X-Requested-With');

            var obj = Y.io(
                //this is a specific Pipes feed, populated with cycling news:
                "/moodle/theme/acebit/api/ccs.php?action=countFriendInvitation",
                cfg
            );
        }

        countFriendInvitation();
        window.setInterval(countFriendInvitation, 60000);
    }
);
