<?php

class acebit_user_picture extends user_picture {

    public function get_url(moodle_page $page, renderer_base $renderer = null) {
        global $DB;

        $user = $this->user;
        if (empty($user->username)) {
            $user = $DB->get_record('user', array('id' => $user->id));
        }

        $url = 'http://api.info.bit.edu.cn/registeredPerson/queryCustomAvatar?num='.$user->username;
        if (!empty($this->size)) {
            $url .= '&size='.$this->size;
        }

        $rst = acebit_diapi_get($url);
        $json = json_decode($rst);

        if (!empty($json->url)) {
            return $json->url;
        } else {
            $url = 'http://api.info.bit.edu.cn/registeredPerson/queryPhoto?num='.$user->username;
            $rst = acebit_diapi_get($url);
            $json = json_decode($rst);

            if (!empty($json->url)) {
                return $json->url;
            } else {
                return parent::get_url($page, $renderer);
            }
        }
    }

}

