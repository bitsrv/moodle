<?php

class theme_acebit_core_course_renderer extends core_course_renderer {

    /**
     * Renders course info box.
     *
     * @param stdClass|course_in_list $course
     * @return string
     */
    public function course_info_box(stdClass $course) {
        $content = '';
        $content .= $this->output->box_start('generalbox info');
        $chelper = new coursecat_helper();
        $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED);
        $content .= parent::coursecat_coursebox($chelper, $course);
        $content .= $this->output->box_end();
        return $content;
    }

    protected function coursecat_courses(coursecat_helper $chelper, $courses, $totalcount = null) {
        global $CFG;

        $content = parent::coursecat_courses($chelper, $courses, $totalcount);
        if ($content == '') return $content;

        $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8"); 

        libxml_use_internal_errors(true);

        $xml = new DOMDocument;
        $xml->loadHTML($content);

        $xsl = new DOMDocument;
        $xsl->load($CFG->dirroot.'/theme/acebit/xsl/coursecat_courses.xsl');

        $proc = new XSLTProcessor;
        $proc->importStyleSheet($xsl); // attach the xsl rules

        return $proc->transformToXML($xml);
    }

    /**
     * Displays one course in the list of courses.
     *
     * This is an internal function, to display an information about just one course
     * please use {@link core_course_renderer::course_info_box()}
     *
     * @param coursecat_helper $chelper various display options
     * @param course_in_list|stdClass $course
     * @param string $additionalclasses additional classes to add to the main <div> tag (usually
     *    depend on the course position in list - first/last/even/odd)
     * @return string
     */
    protected function coursecat_coursebox(coursecat_helper $chelper, $course, $additionalclasses = '') {
        global $CFG, $OUTPUT;

        $content = parent::coursecat_coursebox($chelper, $course, $additionalclasses);
        if ($content == '') return $content;

        $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8"); 

        libxml_use_internal_errors(true);

        $xml = new DOMDocument;
        $xml->loadHTML($content);

        $xsl = new DOMDocument;
        $xsl->load($CFG->dirroot.'/theme/acebit/xsl/coursecat_coursesbox.xsl');

        $proc = new XSLTProcessor;
        $proc->setParameter('', 'default_course_cover', $OUTPUT->pix_url('course_cover', 'theme'));
        $proc->importStyleSheet($xsl); // attach the xsl rules

        return $proc->transformToXML($xml);
    }

}
