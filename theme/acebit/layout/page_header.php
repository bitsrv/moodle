<header id="page-header" class="clearfix">
    <nav class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></nav>
    <?php echo html_writer::tag('img', '', array('src' => $OUTPUT->pix_url('moodle-icon', 'theme'), 'width' => 64, 'height' => 64, 'class' => 'pull-left')); ?>
    <?php echo $html->heading; ?>
    <div id="page-navbar">
        <?php echo $OUTPUT->navbar(); ?>
    </div>
    <div id="course-header">
        <?php echo $OUTPUT->course_header(); ?>
    </div>
</header>
