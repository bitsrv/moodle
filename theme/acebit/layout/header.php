<header role="banner" class="navbar navbar-fixed-top<?php echo $html->navbarclass ?>">
    <nav role="navigation" class="navbar-inner">
        <div class="container-fluid">
            <?php echo html_writer::tag('img', '', array('src' => $OUTPUT->pix_url('bit-logo', 'theme'), 'width' => 36, 'class' => 'pull-left', 'id' => 'logo')); ?>
            <a class="brand" href="http://online.bit.edu.cn"><small>北理在线</small></a>

            <a class="btn btn-navbar" data-toggle="workaround-collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="nav-collapse collapse">
                <?php echo $OUTPUT->custom_menu(); ?>

                <ul class="nav ace-nav pull-right">
                    <li><?php echo $OUTPUT->page_heading_menu(); ?></li>

                    <?php if ($USER->id > 0): ?>
                    <li class="green" id="headerMessage"></li>
                    <script id="header-message-template" type="text/x-handlebars-template">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="icon-envelope"></i>
                            <span class="badge badge-success">{{count}}</span>
                        </a>

                        <ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-closer">
                            {{#messages}}
                            <li>
                                <a href="http://online.bit.edu.cn/ccs/message/show.do?message.id={{messageId}}">
                                    {{pic}}
                                    <span class="msg-body">
                                        <span class="msg-title">
                                            <span class="blue">{{fullname}}:</span>
                                            {{subject}}
                                        </span>

                                        <span class="msg-time">
                                            <i class="icon-time"></i>
                                            <span>{{fromNow}}</span>
                                        </span>
                                    </span>
                                </a>
                            </li>
                            {{/messages}}

                            <li>
                                <a href="http://online.bit.edu.cn/ccs/message/inbox.do">
                                    查看所有站内信
                                    <i class="icon-arrow-right"></i>
                                </a>
                            </li>
                        </ul>
                    </script>

                    <li class="grey" id="headerRequest"></li>
                    <script id="header-request-template" type="text/x-handlebars-template">
                        <a href="http://online.bit.edu.cn/ccs/friend/request.do">
                            <i class="icon-user"></i>
                            <span class="badge badge-grey">{{count}}</span>
                        </a>
                    </script>

                    <script id="header-notification-template" type="text/x-handlebars-template">
                    <li class="purple" id="headerNotification">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="icon-bell-alt"></i>
                            <span class="badge badge-important">{{count}}</span>
                        </a>
                    </li>
                    </script>

                    <li class="light-blue" id="headerProfile">
                        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                            <?php echo $OUTPUT->user_picture($USER, array('size'=>'64', 'class'=>'nav-user-photo', 'link' => false)); ?>
                            <span class="user-info">
                                <small><?php echo $USER->username; ?></small>
                                <?php echo fullname($USER); ?>
                            </span>
                            <i class="icon-caret-down"></i>
                        </a>

                        <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
                            <li><?php echo html_writer::link($CFG->wwwroot.'/user/editadvanced.php?id='.$USER->id, '<i class="icon-cog"></i>'.get_string('settings', 'moodle')); ?></li>
                            <li><?php echo html_writer::link('https://login.bit.edu.cn/profile/passwords', '<i class="icon-lock"></i>'.get_string('changepassword', 'moodle')); ?></li>
                            <li class="divider"></li>
                            <li><?php echo html_writer::link($CFG->wwwroot.'/login/logout.php?sesskey='.sesskey(), '<i class="icon-off"></i>'.get_string('logout', 'moodle')); ?></li>
                        </ul>
                    </li>

                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>
</header>
