<?php

define('AJAX_SCRIPT', true);

require('../../../config.php');
require('../lib.php');

$PAGE->set_url('/api/ccs.php');
$PAGE->set_context(context_system::instance());

require_login();

$action = required_param('action', PARAM_CLEAN);

if ($action == 'countMessageUnread') {
    $params = array('method' => 'message.getUnreadCount', 'loginName' => $USER->username);
    $json = acebit_ccs_get($params);
    $json = json_decode($json); 
    $result = array('num' => $USER->username, 'count' => $json->unReadCount);
    echo json_encode($result);
}

else if ($action == 'listMessages') {
    // 获取未读站内信数
    $params = array('method' => 'message.getUnreadCount', 'loginName' => $USER->username);
    $json = acebit_ccs_get($params);
    $json = json_decode($json); 
    $result = array('num' => $USER->username, 'count' => $json->unReadCount);

    $messages = array();
    if ($result['count'] > 0) {
        // 获取站内信列表
        $params = array('method' => 'message.getMessageList', 'loginName' => $USER->username, 'readType' => 0, 'start' => 0, 'num' => 5);
        
        $json = acebit_ccs_get($params);
        $json = json_decode($json); 
        foreach ($json->messageList as $r) {
            $messages[] = array(
                'messageId' => $r[0]->id,
                'subject' => $r[0]->subject,
                'time' => $r[0]->createTime->time,
                'fullname' => $r[3]->lastName,
                'fromUsername' => $r[3]->loginName
            );
        }
    }
    $result['messages'] = $messages;

    echo json_encode($result);
}

else if ($action == 'countFriendInvitation') {
    $params = array('method' => 'friend.getInvitationsCount', 'loginNames' => $USER->username);
    $json = acebit_ccs_get($params);
    $json = json_decode($json); 
    $result = array('num' => $USER->username, 'count' => $json->invitationsCount);
    echo json_encode($result);
}

else if ($action == 'countCommentUnread') {
    $params = array('method' => 'comment.getUnreadCountComment', 'loginNames' => $USER->username);
    $json = acebit_ccs_get($params);
    $json = json_decode($json); 
    $result = array('num' => $USER->username, 'count' => $json->unReadCountComment);
    echo json_encode($result);
}
