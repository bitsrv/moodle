<?php

require('../../config.php');

$courseid = required_param('courseid', PARAM_INT);

$course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);

require_login($course);

$return = new moodle_url('/enrol/instances.php', array('id'=>$course->id));
if (!enrol_is_enabled('database')) {
    redirect($return);
}

if ($DB->record_exists('enrol', array('courseid'=>$courseid, 'enrol'=>'database'))) {
    return NULL;
}

$plugin = enrol_get_plugin('database');
$plugin->add_instance($course);
redirect($return);
